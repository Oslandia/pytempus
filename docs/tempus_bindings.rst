Tempus main bindings
====================

This page contains a definition of the existing wrappers that were defined for
a range of seminal `Tempus` functions. These wrappers are stored in module
`tempus.__init__`.

Load a plugin
-------------

.. autofunction:: tempus.load_plugin
		  
Load a graph
------------

.. autofunction:: tempus.load_graph
		  
Do routing requests
-------------------

This class is defined to contain the routing request results. It is a wrapper that allow to consider the request itself associated with its result.

.. autoclass:: tempus.ResultWrapper
	       :members: __getitem__, __len__

A binding function `request` is defined by exploiting this class as follows:

.. autofunction:: tempus.request
