.. pytempus_documentation documentation master file, created by
   sphinx-quickstart on Mon Jan 22 10:32:32 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pytempus's documentation!
====================================

Getting started
---------------
.. _Tempus: http://ifsttar.github.io/Tempus/

This project provides Python bindings for the Tempus_ framework and some example codes to illustrate the way it should be used.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :name: mastertoc
   :glob:

   tempus_bindings
   samples/isochrone_plugin
   samples/utils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
