#ifndef PYTEMPUS_COMMON_H
#define PYTEMPUS_COMMON_H

#include <list>

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/optional.hpp>

#include <tempus/base.hh>
#include <tempus/datetime.hh>
#include <tempus/variant.hh>
#include <datetime.h>

namespace bp = boost::python;

/* Helper to register std::vector<T> <-> python list[T] */
template<typename containedType>
struct custom_vector_to_list{
    static PyObject* convert(const std::vector<containedType>& v){
        bp::list ret; BOOST_FOREACH(const containedType& e, v) ret.append(e);
        return bp::incref(ret.ptr());
    }
};
template<typename containedType>
struct custom_vector_from_seq{
    custom_vector_from_seq(){ bp::converter::registry::push_back(&convertible,&construct,bp::type_id<std::vector<containedType> >()); }
    static void* convertible(PyObject* obj_ptr){
        // the second condition is important, for some reason otherwise there were attempted conversions of Body to list which failed afterwards.
        if(!PySequence_Check(obj_ptr) || !PyObject_HasAttrString(obj_ptr,"__len__")) return 0;
        return obj_ptr;
    }
    static void construct(PyObject* obj_ptr, bp::converter::rvalue_from_python_stage1_data* data){
         void* storage=((bp::converter::rvalue_from_python_storage<std::vector<containedType> >*)(data))->storage.bytes;
         new (storage) std::vector<containedType>();
         std::vector<containedType>* v=(std::vector<containedType>*)(storage);
         int l=PySequence_Size(obj_ptr); if(l<0) abort();
         v->reserve(l); for(int i=0; i<l; i++) { v->push_back(bp::extract<containedType>(PySequence_GetItem(obj_ptr,i))); }
         data->convertible=storage;
    }
};


/* Helper to register std::list<T> <-> python list[T] */
template<typename containedType>
struct custom_list_to_list{
    static PyObject* convert(const std::list<containedType>& v){
        bp::list ret; BOOST_FOREACH(const containedType& e, v) ret.append(e);
        return bp::incref(ret.ptr());
    }
};
template<typename containedType>
struct custom_list_from_seq{
    custom_list_from_seq(){
     bp::converter::registry::push_back(&convertible,&construct,bp::type_id<std::list<containedType> >()); }
    static void* convertible(PyObject* obj_ptr){
        // the second condition is important, for some reason otherwise there were attempted conversions of Body to list which failed afterwards.
        if(!PySequence_Check(obj_ptr) || !PyObject_HasAttrString(obj_ptr,"__len__")) return 0;
        return obj_ptr;
    }
    static void construct(PyObject* obj_ptr, bp::converter::rvalue_from_python_stage1_data* data){
         void* storage=((bp::converter::rvalue_from_python_storage<std::list<containedType> >*)(data))->storage.bytes;
         new (storage) std::list<containedType>();
         std::list<containedType>* v=(std::list<containedType>*)(storage);
         int l=PySequence_Size(obj_ptr); if(l<0) abort(); for(int i=0; i<l; i++) { v->push_back(bp::extract<containedType>(PySequence_GetItem(obj_ptr,i))); }
         data->convertible=storage;
    }
};

/* Helpers to declare local variables from get/set functions */
#define GET(CLASS, TYPE, name) \
    const TYPE& (CLASS::*name ## l_get)() const = &CLASS::name; \
    auto name ## _get = make_function(name ## l_get, bp::return_value_policy<bp::copy_const_reference>());

#define GET_NON_CONST(CLASS, TYPE, name) \
    TYPE (CLASS::*name ## l_get)() const = &CLASS::name; \
    auto name ## _get = make_function(name ## l_get, bp::return_value_policy<bp::return_by_value>());

#define GET_SET(CLASS, TYPE, name) \
    GET(CLASS, TYPE, name) \
    void (CLASS::*name ## _set)(const TYPE&) = &CLASS::set_ ## name;

#define GET_NON_CONST_SET(CLASS, TYPE, name) \
    GET_NON_CONST(CLASS, TYPE, name) \
    void (CLASS::*name ## _set)(const TYPE&) = &CLASS::set_ ## name;

/* Macro to convert iterator pairs to Python iterators */
#define BIND_ITERATOR(TYPE, NAME)                            \
    bp::class_<std::pair<TYPE, TYPE>>(NAME, bp::no_init)                \
        .def("__iter__", bp::range(&std::pair<TYPE, TYPE>::first, &std::pair<TYPE,TYPE>::second));

/* Helpers to bind function returning std::unique_ptr */
template <typename T,
          typename C,
          typename ...Args>
bp::object adapt_unique(std::unique_ptr<T> (C::*fn)(Args...))
{
  return bp::make_function(
      [fn](C& self, Args... args) { return (self.*fn)(args...).release(); },
      bp::return_value_policy<bp::manage_new_object>(),
      boost::mpl::vector<T*, C&, Args...>()
    );
}

template <typename T,
          typename ...Args>
bp::object adapt_unique(std::unique_ptr<T> (*fn)(Args...))
{
  return bp::make_function(
      [fn](Args... args) { return (*fn)(args...).release(); },
      bp::return_value_policy<bp::manage_new_object>(),
      boost::mpl::vector<T*, Args...>()
    );
}

template <typename T,
          typename C,
          typename ...Args>
bp::object adapt_unique_const(std::unique_ptr<T> (C::*fn)(Args...) const)
{
  return bp::make_function(
      [fn](C& self, Args... args) { return (self.*fn)(args...).release(); },
      bp::return_value_policy<bp::manage_new_object>(),
      boost::mpl::vector<T*, C&, Args...>()
    );
}

template <typename T,
          typename C,
          typename ...Args>
bp::object adapt_unique_by_value(std::unique_ptr<T> (C::*fn)(Args...))
{
  return bp::make_function(
      [fn](C& self, Args... args) {
        std::unique_ptr<T> p = (self.*fn)(args...);
        T copy(*p);
        return copy;
      },
      bp::return_value_policy<bp::return_by_value>(),
      boost::mpl::vector<T, C&, Args...>()
    );
}


template<typename K, typename V, typename T>
struct map_from_python {
    map_from_python() {
        bp::converter::registry::push_back(
            &convertible,
            &construct,
            bp::type_id<T>());
    }

    static void* convertible(PyObject* obj_ptr) {
        if (!PyMapping_Check(obj_ptr) || !PyMapping_Keys(obj_ptr)) {
            return nullptr;
        } else {
            return obj_ptr;
        }
    }

    static void construct(
        PyObject* obj_ptr,
        bp::converter::rvalue_from_python_stage1_data* data) {

        PyObject* keys = PyMapping_Keys(obj_ptr);
        PyObject* it = PyObject_GetIter(keys);
        if (!it) {
            std::cerr << "Invalid iterator" << std::endl;
            return;
        }

        void* storage = (
            (bp::converter::rvalue_from_python_storage<T>*)
            data)->storage.bytes;

        new (storage) T();
        data->convertible = storage;

        T* result = static_cast<std::map<K,V>*>(storage);

        construct_in_object(obj_ptr, result);
    }

    static void construct_in_object(PyObject* obj_ptr, T* result) {
        PyObject* keys = PyMapping_Keys(obj_ptr);
        PyObject* it = PyObject_GetIter(keys);

        PyObject* key;
        while ((key = PyIter_Next(it))) {
            PyObject* value = PyObject_GetItem(obj_ptr, key);
            if (!value) {
                throw std::runtime_error("Error: invalid value");
            }

            K k = bp::extract<K>(key);
            V v = bp::extract<V>(value);
            result->insert(std::make_pair(k, v));
        }
    }
};


template<typename K, typename V>
struct map_to_python{
    static PyObject* convert(const std::map<K, V>& v){
        bp::dict ret;
        for (auto it= v.begin(); it!=v.end(); ++it) {
            ret[it->first] = it->second;
        }
        return bp::incref(ret.ptr());
    }
};

/* std::pair<T1, T2> -> python tuple */

template<typename T1, typename T2>
struct pair_to_python {
    static PyObject* convert(const std::pair<T1, T2>& pair)
    {
        return bp::incref(bp::make_tuple(pair.first, pair.second).ptr());
    }
};

/** boost::posix::ptime <-> datetime */
struct date_to_python_converter
{
    static PyObject* convert(Tempus::DateTime value)
    {
        if (value.is_not_a_date_time())
            return Py_None;
 
        PyDateTime_IMPORT;
 
        return PyDateTime_FromDateAndTime(
                                          static_cast<int>(value.date().year()),
                                          static_cast<int>(value.date().month()),
                                          static_cast<int>(value.date().day()),
                                          static_cast<int>(value.time_of_day().hours()),
                                          static_cast<int>(value.time_of_day().minutes()),
                                          static_cast<int>(value.time_of_day().seconds()),
                                          static_cast<int>(value.time_of_day().total_microseconds() - value.time_of_day().total_seconds() * 1000000L)
                                          );
    }
};
 
struct date_from_python_converter
{
    date_from_python_converter()
    {
        bp::converter::registry::push_back(&is_convertible, &convert,bp::type_id<Tempus::DateTime>());
    }

    static void* is_convertible(PyObject* obj_ptr)
    {
        assert(obj_ptr);
 
        if (obj_ptr == Py_None)
            return obj_ptr;
 
        PyDateTime_IMPORT;
 
        if (PyDateTime_Check(obj_ptr))
            return obj_ptr;
 
        return NULL;
    }
 
    static void convert(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data)
    {
        assert(obj_ptr);
 
        void* const storage = reinterpret_cast<boost::python::converter::rvalue_from_python_storage<Tempus::DateTime>*>(data)->storage.bytes;
 
        if (obj_ptr == Py_None)
        {
            new (storage) Tempus::DateTime();
        } else
        {
            PyDateTime_IMPORT;
            PyDateTime_DateTime* dt_ptr = reinterpret_cast<PyDateTime_DateTime*>(obj_ptr);
 
            const int year = PyDateTime_GET_YEAR(dt_ptr);
            const int month = PyDateTime_GET_MONTH(dt_ptr);
            const int day = PyDateTime_GET_DAY(dt_ptr);
            const int hour  = PyDateTime_DATE_GET_HOUR(dt_ptr);
            const int minute  = PyDateTime_DATE_GET_MINUTE(dt_ptr);
            const int second = PyDateTime_DATE_GET_SECOND(dt_ptr);
            const int microsecond = PyDateTime_DATE_GET_MICROSECOND(dt_ptr);
 
            new (storage) Tempus::DateTime(boost::gregorian::date(year, month, day), boost::posix_time::time_duration(hour, minute, second, 0) + boost::posix_time::microseconds(microsecond));
        }
 
        data->convertible = storage;
    }
};

/** http://stackoverflow.com/questions/26497922/how-to-wrap-a-c-function-that-returns-boostoptionalt */

namespace detail {

/// @brief Type trait that determines if the provided type is
///        a boost::optional.
template <typename T>
struct is_optional : boost::false_type {};

template <typename T>
struct is_optional<boost::optional<T> > : boost::true_type {};

/// @brief Type used to provide meaningful compiler errors.
template <typename>
struct return_optional_requires_a_optional_return_type {};

/// @brief ResultConverter model that converts a boost::optional object to
///        Python None if the object is empty (i.e. boost::none) or defers
///        to Boost.Python to convert object to a Python object.
template <typename T>
struct to_python_optional
{
  /// @brief Only supports converting Boost.Optional types.
  /// @note This is checked at runtime.
  bool convertible() const { return detail::is_optional<T>::value; }

  /// @brief Convert boost::optional object to Python None or a
  ///        Boost.Python object.
  PyObject* operator()(const T& obj) const
  {
    namespace python = boost::python;
    python::object result =
      obj                      // If boost::optional has a value, then
        ? python::object(*obj) // defer to Boost.Python converter.
        : python::object();    // Otherwise, return Python None.

    // The python::object contains a handle which functions as
    // smart-pointer to the underlying PyObject.  As it will go
    // out of scope, explicitly increment the PyObject's reference
    // count, as the caller expects a non-borrowed (i.e. owned) reference.
    return python::incref(result.ptr());
  }

  /// @brief Used for documentation.
  const PyTypeObject* get_pytype() const { return 0; }
};

} // namespace detail

/// @brief Converts a boost::optional to Python None if the object is
///        equal to boost::none.  Otherwise, defers to the registered
///        type converter to returs a Boost.Python object.
struct return_optional
{
  template <class T> struct apply
  {
    // The to_python_optional ResultConverter only checks if T is convertible
    // at runtime.  However, the following MPL branch cause a compile time
    // error if T is not a boost::optional by providing a type that is not a
    // ResultConverter model.
    typedef typename boost::mpl::if_<
      detail::is_optional<T>,
      detail::to_python_optional<T>,
      detail::return_optional_requires_a_optional_return_type<T>
    >::type type;
  }; // apply
};   // return_optional

struct Variant_to_python{
    static PyObject* convert(const Tempus::Variant& v){
        bp::object ret;
        switch (v.type()) {
            case Tempus::VariantType::BoolVariant:
                ret = boost::python::object(v.as<bool>());
                break;
            case Tempus::VariantType::IntVariant:
                ret = boost::python::object(v.as<int64_t>());
                break;
            case Tempus::VariantType::FloatVariant:
                ret = boost::python::object(v.as<double>());
                break;
            case Tempus::VariantType::StringVariant:
                ret = boost::python::object(v.as<std::string>().c_str());
                break;
            default:
                return nullptr;
        }
        return bp::incref(ret.ptr());
    }
};


struct Variant_from_python {
    Variant_from_python() {
      bp::converter::registry::push_back(
        &convertible,
        &construct,
        bp::type_id<Tempus::Variant>());
    }

    static void* convertible(PyObject* obj_ptr) {
        if (
            #if PY_MAJOR_VERSION >= 3
            !PyUnicode_Check(obj_ptr) &&
            !PyLong_Check(obj_ptr) &&
            #else
            !PyBytes_Check(obj_ptr) &&
            !PyInt_Check(obj_ptr) &&
            #endif
            !PyBytes_Check(obj_ptr) &&
            !PyBool_Check(obj_ptr) &&
            !PyFloat_Check(obj_ptr)) {
            return nullptr;
        } else {
            return obj_ptr;
        }
    }

    static void construct(
        PyObject* obj_ptr,
        bp::converter::rvalue_from_python_stage1_data* data) {
        Tempus::Variant v = construct(obj_ptr);

        void* storage = (
            (bp::converter::rvalue_from_python_storage<Tempus::Variant>*)
            data)->storage.bytes;

        new (storage) Tempus::Variant(v);
        data->convertible = storage;
    }

    static Tempus::Variant construct(PyObject* obj_ptr) {
        #if PY_MAJOR_VERSION >= 3
        if (PyUnicode_Check(obj_ptr)) {
            return Tempus::Variant::from_string(std::string(PyUnicode_AsUTF8(obj_ptr)));
        }
        #else
        if (PyBytes_Check(obj_ptr)) {
            return Tempus::Variant::from_string(std::string(PyString_AsString(obj_ptr)));
        }
        #endif
        else if (PyBool_Check(obj_ptr)) {
            return Tempus::Variant::from_bool(obj_ptr == Py_True);
        }
        #if PY_MAJOR_VERSION >= 3
        else if (PyLong_Check(obj_ptr))
        #else
        else if (PyInt_Check(obj_ptr))
        #endif
        {
            return Tempus::Variant::from_int(PyLong_AsLong(obj_ptr));
        } else if (PyFloat_Check(obj_ptr)) {
            return Tempus::Variant::from_float(PyFloat_AsDouble(obj_ptr));
        } else {
            throw std::runtime_error("Cannot create Variant from obj");
        }
    }
};

#endif

