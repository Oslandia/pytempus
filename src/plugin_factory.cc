#include "common.h"
#include <tempus/plugin_factory.hh>

namespace bp = boost::python;

void register_external_plugin(
    Tempus::PluginFactory* plugin_factory,
    bp::object create,
    bp::object options,
    bp::object caps,
    bp::object name,
    bp::object compatible_routing_data)
{
    auto
        create_fn = [create](Tempus::ProgressionCallback& p, const Tempus::VariantMap& opt, const Tempus::RoutingData* rd) -> Tempus::Plugin* {
                        bp::object do_not_delete_me_please (create(p, opt, rd));
                        bp::incref(do_not_delete_me_please.ptr());
                        return bp::extract<Tempus::Plugin*>(do_not_delete_me_please);
                    };

    auto
     options_fn = [options]() {
        bp::object do_not_delete_me_please (options());
        bp::incref(do_not_delete_me_please.ptr());

        // Aha, nice try. We can't use this ...
        // return bp::extract<Tempus::Plugin::Capabilities*>(do_not_delete_me_please);

        // because there's a of a subtle difference: extract<> needs a bp::class_<> declaration
        // to be able to use python->c++ converters we registered.
        // As OptionDescriptionList is a simple typedef to std::map<> + specific converters we need to
        // hack a bit to get boost|python to play nice with us...

        // So let's try to explicitely use the converter
        typedef map_from_python<std::string, Tempus::Plugin::OptionDescription, Tempus::Plugin::OptionDescriptionList> Conv;
        if (Conv::convertible(do_not_delete_me_please.ptr())) {
            Tempus::Plugin::OptionDescriptionList* result = new Tempus::Plugin::OptionDescriptionList();
            Conv::construct_in_object(do_not_delete_me_please.ptr(), result);
            return result;
        } else {
            throw std::runtime_error( "Cannot convert to C++ OptionDescriptionList" );
        }
    };

    auto
     caps_fn = [caps]() {
        bp::object do_not_delete_me_please (caps());
        bp::incref(do_not_delete_me_please.ptr());
        return bp::extract<Tempus::Plugin::Capabilities*>(do_not_delete_me_please);
    };

    auto
     name_fn = [name]() { return bp::extract<const char*>(name()); };

    auto
     compatible_routing_data_fn = [compatible_routing_data]() { return bp::extract<const char*>(compatible_routing_data()); };

    plugin_factory->register_external_plugin(create_fn, options_fn, caps_fn, name_fn, compatible_routing_data_fn);
}

void export_PluginFactory() {
    bp::class_<Tempus::PluginFactory, boost::noncopyable>("PluginFactory", bp::no_init)
        .def("plugin_list", &Tempus::PluginFactory::plugin_list)
        .def("create_plugin", adapt_unique(&Tempus::PluginFactory::create_plugin))
        .def("load_plugin_module", &Tempus::PluginFactory::load_plugin_module)
        .def("unload_plugin_module", &Tempus::PluginFactory::unload_plugin_module)
        .def("register_external_plugin", &register_external_plugin)
        .def("plugin_capabilities", &Tempus::PluginFactory::plugin_capabilities)
        .def("option_descriptions", &Tempus::PluginFactory::option_descriptions)
        .def("instance", &Tempus::PluginFactory::instance, bp::return_value_policy<bp::reference_existing_object>()).staticmethod("instance")
    ;
}

