import os
import sys

import pytempus
from pytempus import *

# symbols from this file + symbols from pytempus
__all__ = ['load_plugin', 'load_graph', 'request'] + \
          ['Base',
           'Cost',
           'IsochroneValue',
           'MMEdge',
           'MMVertex',
           'Multimodal',
           'POI',
           'Plugin',
           'PluginFactory',
           'PluginRequest',
           'Point2D',
           'Point3D',
           'ProgressionCallback',
           'PublicTransport',
           'Request',
           'ResultElement',
           'Road',
           'Roadmap',
           'RoadmapIterator',
           'RoutingData',
           'TextProgression',
           'TransportMode',
           'TransportModeEngine',
           'TransportModeId',
           'TransportModeRouteType',
           'TransportModeSpeedRule',
           'TransportModeTollRule',
           'TransportModeTrafficRule',
           'cost_name',
           'cost_unit',
           'distance',
           'distance2',
           'edge',
           'get_total_costs',
           'init',
           'load_routing_data']

if sys.platform.startswith('linux'):
    TEMPUS_PLUGINS_DIR = os.getenv('TEMPUS_PLUGINS_DIR', '/usr/local/lib/tempus')
    TEMPUS_PLUGIN_PREFIX = os.getenv('TEMPUS_PLUGIN_PREFIX', 'lib')
    TEMPUS_PLUGIN_SUFFIX = os.getenv('TEMPUS_PLUGIN_SUFFIX', '.so')
elif sys.platform == 'win32':
    TEMPUS_PLUGINS_DIR = os.getenv('TEMPUS_PLUGINS_DIR', 'C:\\OSGEO4W64\\apps\\tempus\\bin')
    TEMPUS_PLUGIN_PREFIX = os.getenv('TEMPUS_PLUGIN_PREFIX', '')
    TEMPUS_PLUGIN_SUFFIX = os.getenv('TEMPUS_PLUGIN_SUFFIX', '.dll')

def load_plugin(options, plugin_name=None, plugin_path=None):
    """
    Load a Tempus plugin.

    :param options: dict - Database options in a dictionary; the main entry is `db/options`
    :param plugin_name: object - String designing the name of the plugin that must be used (commonly supported: "sample_road_plugin", "sample_multi_plugin", "dynamic_multi_plugin", "sample_pt_plugin", "isochrone_plugin")
    :param plugin_path: object - Relative path to the plugin sources (alternative to `plugin_path` parameter, it is considered only if plugin_name is null)
    :return: tempus.Plugin - routing plugin
    """
    if plugin_name is None and plugin_path is None:
        raise RuntimeError("You should provide either a plugin name or a plugin path")
    if isinstance(options, str):
        # str => treated as db options
        options = {'db/options': options}
    
    pr = pytempus.TextProgression(50)
    if plugin_name is not None:
        return pytempus.PluginFactory.instance().create_plugin( \
            os.path.join(TEMPUS_PLUGINS_DIR, \
                         TEMPUS_PLUGIN_PREFIX + plugin_name + TEMPUS_PLUGIN_SUFFIX), \
            pr, options)
    if plugin_path is not None:
        return pytempus.PluginFactory.instance().create_plugin( \
                                                              plugin_path,
                                                              pr, options)

def load_graph(options, graph_type="multimodal_graph"):
    """ Load a Tempus graph

    :param options: dict - Database options as a dictionary; the main entry is `db/options`
    :param graph_type: object - String designing the graph type to be used (supported: "multimodal_graph", not yet supported: "ch_graph")
    :return: tempus.<graph_type>.Graph - routing graph
    """
    pr = pytempus.TextProgression(50)
    return pytempus.load_routing_data(graph_type, pr, options)

class ResultWrapper:
    """ Tempus request result wrapper

    Instanciated with a :class:`Tempus.PluginRequest` (containing one or more :class:`tempus.ResultElement`, *i.e.* :class:`tempus.Roadmap` or :class:`tempus.Isochrone`) and a dict containing the request resolution metrics (*i.e* number of iterations, execution time)
    """
    def __init__(self, plugin_request, results):
        """ Class constructor
        """
        self.results_ = results
        self.metrics = plugin_request.metrics()

    def __getitem__(self, key):
        """ Result item getter: consider results elements (of :class:`tempus.ResultElement`)as a list, *i.e.* they must be accessed through an index `key`

        :param key: integer - id of the result object that must be get
        """
        return self.results_[key]

    def __len__(self):
        """ built-in result element length; gives the number of stored items in the request result
        """
        return len(self.results_)

def request(
        plugin,
        plugin_options=None,
        origin=None,
        steps=None,
        destination=None,
        allowed_transport_modes=None,  # at least pedestrian
        criteria=None,  # list of optimizing criteria
        parking_location=None,
        networks=None  # public transport network id
        ):
    """
    Request the Tempus database according to the `plugin` capabilities, to get a shortest path structure between `origin` and `destination` nodes that uses only `allowed_transport_modes`

    :param plugin: :class:`Tempus.Plugin` - Plugin to use for answering the request (ex: isochrone_plugin)
    :param plugin_options: dict - Additional options to feed to the chosen plugin
    :param origin: integer - Id of the origin node
    :param steps: list - destination specifications (time constraint, parking at destination... for the final destination, or intermediary steps)
    :param destination: integer - Id of the destination node
    :param allowed_transportation_modes: list - Id of the allowed transportation modes; for having a mode glossary, please refer to your database (tempus.transport_mode)
    :param criteria: list - Optimization criteria (supported: :class:`tempus.Cost.Duration`; not yet supported: :class:`tempus.Cost.Distance`, :class:`tempus.Cost.Calories`, :class:`tempus.Cost.Carbon`, :class:`tempus.Cost.Elevation`, :class:`tempus.Cost.Landmark`, :class:`tempus.Cost.NumberOfChanges`, :class:`tempus.Cost.PathComplexity`, :class:`tempus.Cost.Price`, :class:`tempus.Cost.Security`, :class:`tempus.Cost.Variability`)
    :param parking_location: integer - Parking node id **(NOT IMPLEMENTED)**
    :param networks: integer - Network id **(NOT IMPLEMENTED)**
    :return: :class:`ResultWrapper` - a routing request result and some running metrics
    """
    req = pytempus.Request()
    req.origin = origin
    if destination is not None:
        req.destination = destination

    if steps:
        for step in steps[:-1]:
            req.add_intermediary_step(step)
        req.set_destination_step(steps[-1])

    for mode in allowed_transport_modes:
        req.add_allowed_mode(mode)
    for criterion in criteria:
        req.add_criterion(criterion)
    r = plugin.request(plugin_options)

    return ResultWrapper(r, r.process(req))
