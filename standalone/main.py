"""
/**
 *   Copyright (C) 2012-2017 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# python main.py -d "dbname=tempus_test_db port=5434" --origin 2533607913 --destination 309437622
# PYTHONPATH=../astar_plugin python main.py --plugin py_astar_road_plugin --db "dbname=tempus_test_db port=5435"

# This is an example of a standalone application using the Tempus Python API
# It is able to load a C++ plugin as well as a Python plugin

from __future__ import print_function

import tempus
import sys
import argparse
import importlib

import psycopg2

def load_plugin(name, progress, options):
    """ Load a Tempus plugin named as `name` with some database `options`;
    by printing loading `progress`

    Parameters
    ----------
    name: object
        Plugin name
    progress: Progress
        Loading progression
    options: dict
        Set of database options
    """
    try:
        print('Trying to load C++ plugin...')
        tempus.PluginFactory.instance().load_plugin_module(name)
        graph = tempus.load_routing_data("multimodal_graph", progress, options)
        plugin = tempus.PluginFactory.instance().create_plugin(name, progress, options, graph)
        print('Created C++ plugin [{}]'.format(plugin.name))
        return graph, plugin
    except RuntimeError as e:
        print(e)
        print('Failed... Now trying python...')
        pass # now try a python module

    try:
        py_plugin = importlib.import_module(name)

        tempus.PluginFactory.instance().register_plugin_fn(py_plugin.create_plugin, py_plugin.options_description, py_plugin.capabilities, py_plugin.name)

        plugin = tempus.PluginFactory.instance().create_plugin(
            name,
            progress,
            options)
        print('Created python plugin [{}]'.format(plugin.name))
        return plugin

    except ImportError as e:
        print('Failed as well. Aborting.')
        raise RuntimeError('Failed to load plugin {}'.format(name))


def road_node_id_from_coordinates(cur, pt_xy):
    """ Return a node stored into a database accessed through `cur` starting
    from its coordinates `pt_xy`

    Parameters
    ----------
    cur: psycopg2 cursor
        Database connection cursor
    pt_xt: tuple
        (Two floating numbers) point coordinates
    """
    cur.execute("select tempus.road_node_id_from_coordinates(%s,%s)", pt_xy)
    return cur.fetchone()[0]

def main(argv):
    """ Main method of the python API test program

    Arguments:
    argv: dict
        Command-line arguments
    """
    parser = argparse.ArgumentParser(description='Tempus test program')
    parser.add_argument(
        '--db', '-d', type=str,
        dest='db/options',
        help='set database connection options', default='dbname=tempus_test_db')
    parser.add_argument(
        '--plugin', '-p',  type=str,
        help='set the plugin name to launch', default='sample_road_plugin')
    parser.add_argument(
        '--origin_xy', type=str,
        help='set the origin X,Y', default="-1.5444,47.2005")
    parser.add_argument(
        '--destination_xy', type=str,
        help='set the destination X,y', default="-1.5569,47.2161")
    parser.add_argument(
        '--modes', nargs='*',
        help='set the allowed modes (space separated)')
    parser.add_argument(
        '--pvad', type=bool,
        help='set "private vehicule at destination"', default=True)
    parser.add_argument(
        '--depart-after', type=str,
        help='set the time constraint to depart after this date & time')
    parser.add_argument(
        '--arrive-before', type=str,
        help='set the time constraint to arrive before this date & time')
    parser.add_argument(
        '--optimize-distance', type=bool,
        help='optimize distance (instead of duration)', default=False)
    parser.add_argument(
        '--options', nargs='*',
        help='set the plugin options option:type=value (space separated)')
    parser.add_argument(
        '--repeat','-n', type=int,
        help='set the repeat count (for profiling)', default=1)
    parser.add_argument(
        '--load-from','-L', type=str,
        help='set the dump file to load')

    options = vars(parser.parse_args(argv))
    options = {k:v for k,v in vars(parser.parse_args(argv)).items() if v is not
    None}

    plugin_options = {'db/options': options['db/options']}
    # todo parse --options

    # Recover origin and destination coordinates from command-line arguments
    # and build associated origin and destination nodes
    o_xy = [float(x) for x in options["origin_xy"].split(',')]
    d_xy = [float(x) for x in options["destination_xy"].split(',')]
    conn = psycopg2.connect(options["db/options"])
    cur = conn.cursor()
    origin_id = road_node_id_from_coordinates(cur, o_xy) 
    dest_id = road_node_id_from_coordinates(cur, d_xy) 

    # Initialize tempus core
    tempus.init()

    # Initialize a text progress bar
    class Progress(tempus.ProgressionCallback):
        def __call__(self, percent, finished ):
            print ('{}...'.format(percent))

    progression = Progress()

    # Load plugin
    graph, plugin = load_plugin(options['plugin'], progression, plugin_options)

    # Test the plugin with simple requests
    req = tempus.Request()
    req.origin = origin_id
    dest = tempus.Request.Step()
    dest.location = dest_id
    dest.private_vehicule_at_destination = options['pvad']
    tc = tempus.Request.TimeConstraint()
    tc.type = tempus.Request.TimeConstraintType.NoConstraint
    dest.constraint = tc
    req.set_destination_step(dest)
    req.add_allowed_mode(1)

    # Print roadmap
    for i in range(0, options['repeat']):
        p = plugin.request(options)
        result = p.process(req)
        for r in result:
            if r.is_roadmap():
                r = r.roadmap()
                print(r.starting_date_time)
                for s in r:
                    if s.step_type == tempus.Roadmap.Step.StepType.RoadStep:
                        print(s.road_name, s.road_edge_id)
                    print(s.costs())
            else:
                isoc = r.isochrone()
                print(len(isoc))
                for v in isoc[:10]:
                    print(v.x, v.y, v.mode, v.cost)

if __name__ == "__main__":
    """
    Run main method with specific command line arguments
    """
    main(sys.argv[1:])
