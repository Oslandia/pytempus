# coding: utf-8

""" Here are documented some useful functions for testing the pytempus framework.
"""

import os
import psycopg2

g_db_options = os.getenv('TEMPUS_DB_OPTIONS', 'dbname=tempus_test_db')

def road_node_id_from_coordinates(cur, pt_xy):
    """ Return a node stored into the database starting from its coordinates `pt_xy`

    :param cur: psycopg2 cursor - Database connection cursor
    :param pt_xt: tuple - point coordinates, as floating numbers
    :return: integer - id of the node that corresponds to ̀pt_xy`
    """
    cur.execute("select tempus.road_node_id_from_coordinates(%s,%s)", pt_xy)
    return cur.fetchone()[0]

def sample_node(cur):
    """ Return a random node id from the tempus node table

    :param cur: :class:`psycopg2.cursor` - database connexion tool
    :return: tuple - node id
    """
    cur.execute("select id, st_x(geom), st_y(geom) from tempus.road_node order by random();")
    res = cur.fetchone()
    print(res)
    return res[0]

def get_transport_modes(cur):
    """ Get the available transport modes in the current database

    :param cur: :class:`psycopg2.cursor` - database connexion tool
    :return: dict - id and name for each available transport mode
    """
    cur.execute("select id, name from tempus.transport_mode;")
    modes = cur.fetchall()
    return dict(modes)

def browse(vertices, v):
    """ Recursively get the itinerary steps between the path origin and `x`, a
    selected destination node, and print the intermediary node characteristics
    at each iteration

    :param vertices: dict - :class:`tempus.IsochroneValue` indexed by node ids
    :param v: integer - id of the node to browse
    :return: tuple - cost per mode and total waiting time to reach the current destination
    """
    x = vertices[v]
    print(x.uid, (x.predecessor, x.mode, x.cost, x.wait_time))
    total_wait_time = 0.0
    cost_per_mode = {}
    if x.predecessor != x.uid:
        cost_per_mode, total_wait_time = browse(vertices, x.predecessor)
    if x.mode not in cost_per_mode:
        cost_per_mode[x.mode] = 0.0
    total_wait_time += x.wait_time
    cost_per_mode[x.mode] += x.cost
    return cost_per_mode, total_wait_time
