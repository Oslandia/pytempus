"""Test the Tempus isochrone plugin by running a simple isochrone request
starting from a random node in the network

Computing an isochrone needs to compute travel times to reach every other nodes
from the starting node, and to compare these travel times to a fixed time
limit: the isochrone is the set of nodes reachable in this amount of time

"""

from datetime import datetime
import random

import psycopg2

import tempus
from tempus import Cost, Request

import utils

if __name__ == '__main__':
    # initialize the tempus library (mandatory)
    tempus.init()

    # keep a connection to the db
    conn = psycopg2.connect(utils.g_db_options)
    cursor = conn.cursor()

    # load a routing plugin
    plugin = tempus.load_plugin({'db/options': utils.g_db_options},
                                plugin_name="isochrone_plugin")

    # prepare the request
    # test_coordinates = (-1.54427674939367, 47.2010310270336)
    # origin = utils.road_node_id_from_coordinates(cursor, visum_coordinates) #
    # coordinate-based alternative...
    origin = utils.sample_node(cursor) # ...or random alternative
    constraint = \
    Request.TimeConstraint(type=Request.TimeConstraintType.ConstraintAfter,
                           date_time=datetime(2016,10,21,6,43))
    step = Request.Step(constraint=constraint)
    iso_limit = 20.0
    db_modes = utils.get_transport_modes(cursor)
    print("Available transport modes in the database: {}".format(db_modes))
    allowed_modes = [1, 3] # visum : 1=pedestrian, 3=private car
    # or alternatively tempus_test_db : 1=pedestrian, 5= TRAM

    print(("Compute isochrone from node {} with a time threshold "
           "of {} minutes and following modes: {}"
           "").format(origin, iso_limit, [db_modes[k] for k in allowed_modes]))

    # routing request
    results = tempus.request(plugin = plugin,
                             origin = origin,
                             steps = [step],
                             plugin_options = { 'Isochrone/limit' : iso_limit },
                             criteria = [Cost.Duration],
                             allowed_transport_modes = allowed_modes)

    # explore the resulting isochrone
    result_isochrone = results[0].isochrone()
    print("Resulting structure has size = {}".format(len(results)))
    print("Number of reachable nodes: {}".format(len(result_isochrone)))
    print("id, predecessor, x, y")
    print("\n".join(["{},{},{},{}".format(x.uid, x.predecessor, x.x, x.y) for x in results[0].isochrone()]))

    # get roadmap details for isochrone-compliant destination nodes
    vertices = {x.uid: x for x in result_isochrone}
    v = random.choice(range(len(vertices)))
    print("Path between node {} and node {}:".format(origin, v))
    cost_per_mode, total_wait_time = utils.browse(vertices, v)
    print("Waiting time: {:.1f} mins".format(total_wait_time))
    print("Total cost: {:.1f} mins".format(sum(cost_per_mode.values())))
    print("Accumulated costs per mode:")
    print("\n".join("{}: {:.1f} mins".format(k,v) for k,v in cost_per_mode.items()))
