set OSGEO4W_ROOT=C:\osgeo4w64
call c:\osgeo4w64\bin\py3_env.bat
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
:: add python3.dll to the path
set PATH=%PATH%;c:\osgeo4w64\bin
python setup.py build_ext --inplace ^
  -I c:\osgeo4w64\include\boost-1_63 ^
  -L c:\osgeo4w64\lib ^
  -I c:\osgeo4w64\apps\tempus\include ^
  -L c:\osgeo4w64\apps\tempus\lib
:: pytempus.cp36-win_amd64.pyd


